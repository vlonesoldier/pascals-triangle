/**
 * @param {number} numRows
 * @return {number[][]}
 */

let arrPascal = [];

var generate = function (numRows) {

    if (numRows < 1 || numRows > 30) {
        return 0;
    }

    if (numRows === 1)
        arrPascal[numRows - 1] = [1];

    else {
        generate(numRows - 1);

        if (numRows === 2) {
            arrPascal[numRows - 1] = [1, 1]
        }
        else {
            const oneLvlArr = [];

            for (let i = 1; i <= numRows; i++) {

                if (i === 1 || i === numRows)
                    oneLvlArr.push(1);

                else
                    oneLvlArr.push(arrPascal[numRows - 2][i - 2] + arrPascal[numRows - 2][i - 1]);

            }
            arrPascal.push(oneLvlArr);
        }
    }
    return arrPascal;

};

console.log(generate(20))