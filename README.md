# Pascals Triangle

Print off Pascal's Triangle while number of rows is given

Examples:
```sh
Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
```
```sh
Input: numRows = 1
Output: [[1]]
```